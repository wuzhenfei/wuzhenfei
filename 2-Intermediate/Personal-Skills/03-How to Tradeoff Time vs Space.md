# How to Tradeoff Time vs. Space
如何权衡时间与空间

You can be a good programmer without going to college, but you can't be a good intermediate programmer without knowing basic computational complexity theory. You don't need to know 'big O' notation, but I personally think you should be able to understand the difference between 'constant-time','n log n' and 'n squared'. You might be able to intuit how to trade-off time against space without this knowledge, but in its absence you will not have a firm basis for communicating with your colleagues.
你可以成为一个好的程序员不用一定上大学，但你不知道基础的复杂的计算理论你就不能成为一个好的中级程序员。你不需要知道“big o”符号，但我个人认为你应该能够理解“constant-time”，“n log n”和“n squared”的不同。我们可能直觉的知道怎样权衡时间利用空间但没有这方面的知识，但这种缺乏你会没有一个坚实的基础来跟你的同行交流。
In designing or understanding an algorithm, the amount of time it takes to run is sometimes a function of the size of the input. When that is true, we can say an algorithm's worst/expected/best-case running time is 'n log n' if it is proportional to the size ($n$) times the logarithm of the size. The notation and way of speaking can be also be applied to the space taken up by a data structure.
在设计或理解一个算法，大量的时间都花费在调试一个短时间的功能上一个类型的输出。当那是事实的时，我们可以说一个算法的是坏的、期望的、最好的情况运行时间是‘n log n’如果是这个数目与（$n$)乘以这个数的对数是成比例的。

To me, computational complexity theory is beautiful and as profound as physics - and a little bit goes a long way!
对与我来说复杂的计算理论是美丽的对于物理学意义深远的而且还有可能会走的更远。
Time (processor cycles) and space (memory) can be traded off against each other. Engineering is about compromise, and this is a fine example. It is not always systematic. In general, however, one can save space by encoding things more tightly, at the expense of more computation time when you have to decode them. You can save time by caching, that is, spending space to store a local copy of something, at the expense of having to maintain the consistency of the cache. You can sometimes save time by maintaining more information in a data structure. This usually cost a small amount of space but may complicate the algorithm.
时间（处理周期）和空间（内存）可能交替的依靠彼此。工程学是关于协调和这些巧妙的例子。这些并不总是成系统的。总的来说不论如何一个能够紧密的储存编码的内存，要花费更多的计算时间来编译它们。你可以节省编译的时间，这样要占用一些空间来储存这些附近的拷贝，在这些花费中含有不断的维护高速缓存。你可以节省时间来维护更多信息在一个数据系统中。通常这些在空间上花费数量更少的但可能会使算法变的复杂。
Improving the space/time trade-off can often change one or the other dramatically. However, before you work on this you should ask yourself if what you are improving is really the thing that needs the most improvement. It's fun to work on an algorithm, but you can't let that blind you to the cold hard fact that improving something that is not a problem will not make any noticeable difference and will create a test burden.
改善内存与速度的协调一般应该显著的改变其中一个。然而在为这些工作之前你应该问你自己哪个的提升是可能并且有必要的。编一个算法是很有意思的，但是你不能让他蒙蔽你去毫无准备的努力去改善一些实际没有问题的东西最后也没有做出什么显著的改变还将产生测试的负担。
Memory on modern computers appears cheap, because unlike processor time, you can't see it being used until you hit the wall; but then failure is catastrophic. There are also other hidden costs to using memory, such as your effect on other programs that must be resident, and the time to allocate and deallocate it. Consider this carefully before you trade away space to gain speed.
储存器在现代计算机上似乎很便宜，因为不像处理机时间，你不能看到它被使用直到你遇到故障，但是之后会突然的失效。这里也有隐藏的花费在储存器上，就像你作用在其他的程序时的影响那一定是用户和时间来划分和释放。考虑清楚在你花费掉内存来换取时间。
Next [How to Stress Test](04-How to Stress Test.md)
