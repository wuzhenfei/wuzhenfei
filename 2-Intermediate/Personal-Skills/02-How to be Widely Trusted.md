# How to be Widely Trusted
怎样得到广泛的信任

To be trusted you must be trustworthy. You must also be visible. If no one knows about you, no trust will be invested in you. With those close to you, such as your , this should not be an issue. You establish trust by being responsive and informative to those outside your department or team. Occasionally someone will abuse this trust, and ask for unreasonable favours. Don't be afraid of this, just explain what you would have to give up doing to perform the favour.
要想得到信任你必须是可靠地。你也必须是容易被了解的。如果没有一个人了解你，就没有信任赋予给你。这些和你亲近的人，比如你的队友，这可能不是一个问题。你的信任是由于在部门或团队中沟通互动与大量信息处于公开而建立的。偶尔的有些人会滥用这些信任，寻求一些没有理由的支持。不要害怕这些，只要解释什么是你将要放弃而有利于执行的。
Don't pretend to know something that you don't. With people that are not teammates, you may have to make a clear distinction between 'not knowing right off the top of my head' and 'not being able to figure it out, ever.'
不要假装什么你不知道的事。对于那些不是你的队友的人，你应该不得不清楚区分“不知道此刻头上的是什么”和“还不能够弄明白它”。
Next [How to Tradeoff Time vs. Space](03-How to Tradeoff Time vs Space.md)
