# How to Stay Motivated
怎样保持动力

It is a wonderful and surprising fact that programmers are highly motivated by the desire to create artifacts that are beautiful, useful, or nifty. This desire is not unique to programmers nor universal but it is so strong and common among programmers that it separates them from others in other roles.
这个让人兴奋和惊讶的事实是程序员有很高涨的动力是由于他们有创造那些漂亮的有用的精美的产品的欲望。这种欲望不是程序员独有的也不是普遍性的，但是在程序员中是如此强大和普遍从而使得他们从其他的角色中分离了出来。
This has practical and important consequences. If programmers are asked to do something that is not beautiful, useful, or nifty, they will have low morale. There's a lot of money to be made doing ugly, stupid, and boring stuff; but in the end, fun will make the most money for the company.
这些带来了实际的重大的影响。如果程序员被要求去做一些不漂亮、没用也不精美的东西，他们将会没有什么斗志了。有很多钱被用来做丑的愚蠢的和无聊的东西，但是有趣的是到了最后将会为公司赚更多的钱。
Obviously, there are entire industries organized around motivational techniques some of which apply here. The things that are specific to programming that I can identify are:
明显的，整个行业都有在组织周围有动机的技术把其中的一些应用的这里。我能够辨认出对于编程的特定事情
- Use the best language for the job.
  使用适合这份工作的最好的语言
- Look for opportunities to apply new techniques, languages, and technologies.
  寻找机会应用新的方法、语言和技术。
- Try to either learn or teach something, however small, in each project.
   在每个无论多小的计划中试着学或者教一些东西。
Finally, if possible, measure the impact of your work in terms of something that will be personally motivating. For example, when fixing bugs, counting the number of bugs that I have fixed is not at all motivational to me, because it is independent of the number that may still exist, and is also affects the total value I'm adding to my company's customers in only the smallest possible way. Relating each bug to a happy customer, however, *is* personally motivating to me.
最后如果有可能估量你的产品在你的团队某些方面的影响，这些将会是对你个人的激励。例如当要修复漏洞时，计算我需要修复的漏洞数目对我没有一点激励效果，因为这是一个可能依然存在的独立数字，这也是影响我增加到我的公司顾客总数的最小方法。一个满意的顾客会涉及到每一个漏洞然而这就是对于我个人的激励
Next [How to be Widely Trusted](02-How to be  Widely Trusted.md)