# How to Learn New Skills
如何学习新技能
Learning new skills, especially non-technical ones, is the greatest fun of all. Most companies would have better morale if they understood how much this motivates programmers.
学习新技能，尤其是非技术型的那种，是所有之中最有趣的。大多数公司如果理解如何激发程序员那么他们会更有士气的。
Humans learn by doing. Book-reading and class-taking are useful. But could you have any respect for a programmer who had never written a program? To learn any skill, you have to put yourself in a forgiving position where you can exercise that skill. When learning a new programming language, try to do a small project in it before you have to do a large project. When learning to manage a software project, try to manage a small one first.
人通过实践来学习。读书学习和课堂学习是有用的。但是你有个你尊敬他的程序员是不写程序的么？要学习一个技能，你需要把你放在一个宽松的位置你可以来练习这项技能。当学习一个新的编程语言，试着做一个小的项目在你不得不做一个大项目之前。当你学习去管理一个软件工程时，试着从一个小的开始。
A good mentor is no replacement for doing things yourself, but is a lot better than a book. What can you offer a potential mentor in exchange for their knowledge? At a minimum, you should offer to study hard so their time won't be wasted.
一个好的指导是你自己做一件事情这是没有什么可替代的，但是这比一本书好的多。你能提供一个怎样可能的指导来改变他们的认识？至少，你应该让他们努力学习从而他们的时间不会浪费。
Try to get your boss to let you have formal training, but understand that it often not much better than the same amount of time spent simply playing with the new skill you want to learn. It is, however, easier to ask for training than playtime in our imperfect world, even though a lot of formal training is just sleeping through lectures waiting for the dinner party.
试着使你的老板让你有一个正式的培训，但是理解这些没有你把同样时间花在练习你喜欢学习的新技能好。虽然这些寻求培训比在这不完美的时间里娱乐要容易，尽管许多正式的训练都是在演讲中一直睡等待着晚上聚餐。
If you lead people, understand how they learn and assist them by assigning them projects that are the right size and that exercise skills they are interested in. Don't forget that the most important skills for a programmer are not the technical ones. Give your people a chance to play and practice courage, honesty, and communication.
如果你领导其他人，懂得他们是怎样学习和帮助他们设计合适大小的计划同时他们对练习技能感兴趣。不要忘了对于程序员最重要的技能不是那个技术性的一个。给你的员工一个展现的机会和实践的勇气，同时诚实的相互交流。
Next [Learn to Type](07-Learn to Type.md)
